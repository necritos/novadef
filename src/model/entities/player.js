/**
 * Created with JetBrains WebStorm.
 * User: pc
 * Date: 15/11/12
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
Crafty.c("Player", {
    id:null,
    description:null,
    arrBooks:new Array(),
    arrPictures:new Array(),
    arrEnem:new Array(),
    movementSpeed:3,
    energy:100,
    picture:null,
    enemy:null,
    direction:1,
    numM:5,
    punts:900000,
    init:function () {

        this.requires("2D, Canvas, playersprite , SpriteAnimation , Multiway, Keyboard, Collision")
            .attr({w:20, h:20, x:50, y:50})
            .animate("walk_left", 6,0,8)
            .animate("walk_right", 3, 0, 5)
            .animate("walk_up", 9, 0, 11)
            .animate("walk_down", 0, 0, 2)
            .multiway(this.movementSpeed, { /*Enable Movement Control*/
                UP_ARROW:-90,
                DOWN_ARROW:90,
                RIGHT_ARROW:0,
                LEFT_ARROW:180
            })
            .bind("EnterFrame", function () {
                if(this.isDown("LEFT_ARROW")) {
                    if(!this.isPlaying("walk_left"))
                        this.animate("walk_left", 1);
                } else if(this.isDown("RIGHT_ARROW")) {
                    if(!this.isPlaying("walk_right"))
                        this.animate("walk_right", 1);
                } else if(this.isDown("UP_ARROW")) {
                    if(!this.isPlaying("walk_up"))
                        this.stop().animate("walk_up",1);
                } else if(this.isDown("DOWN_ARROW")) {
                    if(!this.isPlaying("walk_down"))
                        this.stop().animate("walk_down", 1);
                }
                if(this.arrEnem.length==0&&this.arrPictures.length==0){
                    pts+=this.punts;
                    Crafty.audio.play("y1",-1);
                  //  Crafty.audio.play("intro",-1);
                    Crafty.scene("good");
                }
                if(this.punts>0)
                    this.punts--;
            })


            .onHit("Book", function (ent) {
                //var book = ent[0].obj;

                //Crafty(player[0]).trigger("book",this);

            })
            .bind("KeyDown", function (e) {
                if (e.keyCode === Crafty.keys.UP_ARROW) {
                    this.direction=3;
                }
                if (e.keyCode === Crafty.keys.LEFT_ARROW) {
                    this.direction=2;
                }
                if (e.keyCode === Crafty.keys.RIGHT_ARROW) {
                    this.direction=1;
                }
                if (e.keyCode === Crafty.keys.DOWN_ARROW) {
                    this.direction=4;
                }
                if (e.keyCode === Crafty.keys.D) {
                    alert(this.books.length);
                }
                if (e.keyCode === Crafty.keys.SPACE) {
                    //alert("XD");
                    if (this.hit("Picture")) {
                        //alert("XD");

                        if (this.picture != null) {


                            if(this.picture.p_state==true){
                                this.arrPictures.pop();
                                pts+=100;
                                Crafty.audio.play("y1",-1);
                            }
                            this.picture.p_state=false;
                            $("#marcas").html(this.arrPictures.length);

                            $("#puntaje").html(pts);
                            //alert("XD");
                        }
                        var e=this.picture;
                        this.picture=null;
                        e.destroy();

                    }

                }
                if (e.keyCode === Crafty.keys.X) {
                    if (this.numM >= 1) {
                        var m = Crafty.e("Manacles").attr({x:this.x, y:this.y}).setDirection(this.direction);

                        this.numM--;
                    }
                }

            })
            .bind('Moved', function (from) {
                if (this.hit('pared10x600')) {
                    this.attr({x:from.x, y:from.y});
                }
                if (this.hit('pared20x300')) {
                    this.attr({x:from.x, y:from.y});
                }
                if (this.hit('pared200x20')) {
                    this.attr({x:from.x, y:from.y});
                }
                if (this.hit('pared20x250')) {
                    this.attr({x:from.x, y:from.y});
                }
                if (this.hit('barra')) {
                    this.attr({x:from.x, y:from.y});
                }
                if (this.hit('pared1200x10')) {
                    this.attr({x:from.x, y:from.y});
                }
                if (this.hit('enemy1')) {
                    this.attr({x:from.x, y:from.y});

                }
                if (this.hit('carpetagi')) {
                    this.attr({x:from.x, y:from.y});

                }
                if (this.hit('enemy2')) {
                    this.attr({x:from.x, y:from.y});

                }
                if (this.hit('enemy3')) {
                    this.attr({x:from.x, y:from.y});

                }
                if (this.hit('enemy4')) {
                    this.attr({x:from.x, y:from.y});

                }
                if (this.hit('carpeta1')) {
                    this.attr({x:from.x, y:from.y});

                }
                if (this.hit('carpeta2')) {
                    this.attr({x:from.x, y:from.y});

                }
                if(this.hit("marca")){
                    $("#mensaje").html("Preciosa espacio cuando estes encima de una marca , para borrarla");
                }
            })
            .onHit("Wall", function() {

                switch (this.direction){
                    case 1:
                        this.x-=this.movementSpeed;
                        //this.animate("walk_right", 10);
                        break;
                    case 2:
                        this.x+=this.movementSpeed;
                        //this.animate("walk_left", 10);
                        break;
                    case 3:
                        this.y+=this.movementSpeed;
                        //this.animate("walk_up", 10);
                        break;
                    case 4:
                        this.y-=this.movementSpeed;
                        //this.animate("walk_down", 10);
                        break;
                }
                this.stop();
            })
            .onHit("Picture", function (ent) {
                this.picture = ent[0].obj;

            })

        $("#convencimiento").html(this.energy);
        $("#libros").html(this.arrBooks.length);

    },
    setBook:function (data) {
        this.arrBooks.push(data);
        this.energy+=250;
        $("#convencimiento").html(this.energy);
        $("#libros").html(this.arrBooks.length);
        pts+=150;
        $("#puntaje").html(pts);
    },
    setPictures:function (data) {
        this.arrPictures=data;
    },
    setEnemy:function (data) {
        var ww=data;
        if(ww.energy<this.energy){
            this.energy-=ww.energy;

            $("#convencimiento").html(this.energy);
            ww.destroy();
            pts+=250;
            $("#puntaje").html(pts);
            this.arrEnem.pop();
            $("#enemigos").html(this.arrEnem.length);
            Crafty.audio.play("enem",-1);
        }else{
            Crafty.audio.play("l1",-1);
            Crafty.scene("gameover");
        }
    }

});