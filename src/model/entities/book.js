Crafty.c("Book",{
    id:null,
    description:null,
    puntos:250,
    type:0,
    init:function(){
        this.type=Crafty.math.randomInt(1,3).toString();
        this.requires("2D, Canvas, libro"+this.type+" , Collision")
            .attr({x:60,y:100,w:20,h:20})

        .bind("EnterFrame",function(){
            //animacion
        })

        .onHit("Player",function(ent){
            $("#mensaje").html("Bien ,recuerda que para conseguir mas convencimiento necesitas libros...");
            ent[0].obj.setBook(100);
            this.destroy();
        })
    }
});