/**
 * Created with JetBrains WebStorm.
 * User: pc
 * Date: 15/11/12
 * Time: 12:20 PM
 * To change this template use File | Settings | File Templates.
 */
Crafty.c("Manacles",{
    id:null,
    direction:1,
    dx:0,
    dy:0,
    x_init:0,
    y_init:0,
    speed:1,
    init:function(){

        this.requires("2D, Canvas, Color, Collision")
            .attr({x:60,y:100,w:5,h:5})
            .color("#689474")
            .bind("EnterFrame",function(){
                //animacion
                this.x+=this.dx;
                this.y+=this.dy;
                if(Math.abs(this.x-this.x_init)>=100||Math.abs(this.y-this.y_init)>=100){
                    this.destroy();
                }
            })

            .onHit("Enemy",function(ent){
               // ent[0].obj.capture(100);
                this.destroy();
            })


    },
    setDirection:function(direction){
        this.direction=direction;
        this.x_init=this.x;
        this.y_init=this.y;
        switch(this.direction){
            case 1://derecha
                this.dx=5;
                break;
            case 2://izquierda
                this.dx=-5;
                break;
            case 3://arriba
                this.dy=-5;
                break;
            case 4://abajo
                this.dy=5;
                break;

        }
    }
});