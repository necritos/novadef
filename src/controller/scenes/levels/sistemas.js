/**
 * Created with JetBrains WebStorm.
 * User: pc
 * Date: 15/11/12
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
Crafty.scene("derecho",function(){
    var books=new Array();
    var enemies=new Array();
    var pictures=new Array();
    var posi_pictures=new Array();
    var posi_enemies=new Array();
    var posi_books=new Array();
    var posi_cg=new Array();
    var posi_cgi=new Array();
    var posi_cp=new Array();
    for(var ii=0;ii<12;ii++){
        for(var jj=0;jj<6;jj++){

            Crafty.e("2D, Canvas, piso").attr({x:100*ii,y:100*jj});

        }
    }
    Crafty.e("2D, Canvas, pared1200x10").attr({x:0,y:0});
    Crafty.e("2D, Canvas, pared10x600").attr({x:0,y:0});
    Crafty.e("2D, Canvas, pared1200x10").attr({x:0,y:600});
    Crafty.e("2D, Canvas, pared10x600").attr({x:1200,y:0});


    Crafty.e("2D, Canvas, pared20x300").attr({x:400,y:10});
    Crafty.e("2D, Canvas, pared20x300").attr({x:900,y:10});
    Crafty.e("2D, Canvas, pared200x20").attr({x:420,y:290});
    Crafty.e("2D, Canvas, pared200x20").attr({x:700,y:290});
    Crafty.e("2D, Canvas, pared200x20").attr({x:920,y:290});

    posi_pictures.push({x:300,y:300},{x:950,y:250},{x:50,y:500},{x:1100,y:500});
    posi_books.push({x:100,y:500},{x:200,y:200},{x:550,y:100},{x:750,y:90},{x:1050,y:90},{x:1100,y:90},{x:1150,y:390});
    posi_enemies.push({x:200,y:200},{x:350,y:400},{x:550,y:350},{x:1150,y:500});
    posi_cp.push({x:200,y:600},{x:800,y:600},{x:1000,y:500});
    posi_cg.push({x:10, y:150},{x:110, y:150},{x:210, y:150},
        {x:110, y:230},{x:210, y:230},
        {x:450, y:50},{x:450, y:110},{x:450, y:170},{x:450, y:230},
        {x:600, y:50},{x:600, y:110},{x:600, y:170},{x:600, y:230},
        {x:750, y:50},{x:750, y:110},{x:750, y:170},{x:750, y:230},
        {x:450, y:420},{x:550, y:420},{x:650, y:420},{x:750, y:420},{x:850, y:420},
        {x:450, y:530},{x:650, y:530},{x:550, y:530},{x:750, y:530},{x:850, y:530},{x:950, y:530});
    posi_cgi.push({x:110, y:250},{x:110, y:350},{x:240, y:350},{x:240, y:450},{x:270, y:500},
    {x:450, y:310},
        {x:1040, y:310},{x:1040, y:410});
    for(var pp=0;pp<posi_pictures.length;pp++){
        var p=Crafty.e("Picture").attr( posi_pictures[pp]);
        pictures.push(p);

    }
    for(var bb=0;bb<posi_books.length;bb++){
        var b=Crafty.e("Book").attr( posi_books[bb]);
        books.push(b);
    }


    var player = Crafty.e("Player");
    player.setPictures(pictures);
    player.arrEnem=new Array();
    for(var ee=0;ee<posi_enemies.length;ee++){
        var e=Crafty.e("Enemy").attr( posi_enemies[ee]);

        e.player=player;
        e.energy=400;
        enemies.push(e);
        player.arrEnem.push(1);

    }




    for(var cp=0;cp<posi_cp.length;cp++){
        Crafty.e("2D, Canvas, carpeta1").attr(posi_cp[cp]);


    }
    for(var cg=0;cg<posi_cg.length;cg++){
        Crafty.e("2D, Canvas, carpeta2").attr(posi_cg[cg]);


    }
    for(var cgi=0;cgi<posi_cgi.length;cgi++){
        Crafty.e("2D, Canvas, carpetagi").attr(posi_cgi[cgi]);


    }
    $("#menu").show();
    $("#marcas").html(pictures.length);
    $("#enemigos").html(enemies.length);

    Crafty.e("Camera").camera(player);
    //Crafty.e("Picture").attr({x:500,y:100});

    //e.player=player;
    //e.energy=1040;
    Crafty.audio.play("music_bg",-1);
});