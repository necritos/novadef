/**
 * Created with JetBrains WebStorm.
 * User: pc
 * Date: 15/11/12
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
Crafty.scene("sistemas",function(){
    var books=new Array();
    var enemies=new Array();
    var pictures=new Array();
    var posi_pictures=new Array();
    var posi_enemies=new Array();
    var posi_books=new Array();
    var posi_cg=new Array() ;
    var posi_cp=new Array() ;
    var posi_cgi=new Array();
    for(var ii=0;ii<12;ii++){
        for(var jj=0;jj<6;jj++){

            Crafty.e("2D, Canvas, piso").attr({x:100*ii,y:100*jj});

        }
    }
    Crafty.e("2D, Canvas, pared1200x10").attr({x:0,y:0});
    Crafty.e("2D, Canvas, pared10x600").attr({x:0,y:0});
    Crafty.e("2D, Canvas, pared1200x10").attr({x:0,y:600});
    Crafty.e("2D, Canvas, pared10x600").attr({x:1200,y:0});


    Crafty.e("2D, Canvas, pared200x20").attr({x:10,y:240});
    Crafty.e("2D, Canvas, pared20x250").attr({x:200,y:10});

    Crafty.e("2D, Canvas, pared20x250").attr({x:350,y:10});
    Crafty.e("2D, Canvas, pared20x250").attr({x:750,y:10});
    Crafty.e("2D, Canvas, pared200x20").attr({x:360,y:240});
    Crafty.e("2D, Canvas, pared200x20").attr({x:760,y:240});
    Crafty.e("2D, Canvas, pared200x20").attr({x:860,y:240});
    Crafty.e("2D, Canvas, pared200x20").attr({x:960,y:240});

    Crafty.e("2D, Canvas, pared20x250").attr({x:350,y:350});
    Crafty.e("2D, Canvas, pared20x250").attr({x:650,y:350});
    Crafty.e("2D, Canvas, pared200x20").attr({x:450,y:350});
    Crafty.e("2D, Canvas, pared200x20").attr({x:700,y:350});
    Crafty.e("2D, Canvas, pared200x20").attr({x:900,y:350});
    Crafty.e("2D, Canvas, pared20x250").attr({x:1090,y:350});

    posi_pictures.push({x:100,y:500},{x:150,y:380},{x:1000,y:50});
    posi_books.push({x:510,y:500},{x:240,y:200},{x:540,y:100},{x:910,y:480},{x:1150,y:550},{x:100,y:550},{x:200,y:520},{x:1100,y:60});
    posi_enemies.push({x:310,y:500},{x:780,y:100},{x:800,y:500},{x:1150,y:550},{x:450,y:50});
    posi_cg.push({x:10,y:400},{x:110,y:400},{x:210,y:400});
    posi_cp.push({x:400, y:40},{x:480, y:40},{x:560, y:40},{x:640, y:40} ,
        {x:400, y:100},{x:480, y:100},{x:560, y:100},{x:640, y:100},
        {x:400, y:160},{x:480, y:160},{x:560, y:160},{x:640, y:160},
        {x:800, y:40},{x:880, y:40},{x:960, y:40},{x:1040, y:40} ,{x:1160, y:40},
        {x:800, y:100},{x:880, y:100},{x:960, y:100},{x:1040, y:100}, {x:1160, y:100},
        {x:800, y:160},{x:880, y:160},{x:960, y:160},{x:1040, y:160},{x:1160, y:160});
    posi_cgi.push({x:280, y:430},{x:200, y:270},
        {x:420, y:370},{x:420, y:500},{x:480, y:370},{x:480, y:500},{x:540, y:370},{x:540, y:500},
        {x:880, y:370},{x:880, y:500},{x:820, y:370},{x:820, y:500},{x:760, y:370},{x:760, y:500},
        {x:940, y:370},{x:940, y:500},{x:1000, y:370},{x:1000, y:500},{x:1060, y:370},{x:1060, y:500});

    for(var pp=0;pp<posi_pictures.length;pp++){
        var p=Crafty.e("Picture").attr( posi_pictures[pp]);

        pictures.push(p);

    }
    for(var bb=0;bb<posi_books.length;bb++){
        var b=Crafty.e("Book").attr( posi_books[bb]);
        books.push(b);
    }


    var player = Crafty.e("Player").attr({x:300,y:100});
    player.setPictures(pictures);
    player.arrEnem=new Array();
    for(var ee=0;ee<posi_enemies.length;ee++){
        var e=Crafty.e("Enemy").attr( posi_enemies[ee]);

        e.player=player;
        e.energy=400;
        enemies.push(e);
        player.arrEnem.push(1);

    }




    for(var cp=0;cp<posi_cp.length;cp++){
        Crafty.e("2D, Canvas, carpeta1").attr(posi_cp[cp]);


    }
    for(var cg=0;cg<posi_cg.length;cg++){
        Crafty.e("2D, Canvas, carpeta2").attr(posi_cg[cg]);


    }
    for(var cgi=0;cgi<posi_cgi.length;cgi++){
        Crafty.e("2D, Canvas, carpetagi").attr(posi_cgi[cgi]);


    }
    $("#menu").show();
    $("#marcas").html(pictures.length);
    $("#enemigos").html(player.arrEnem.length);

    Crafty.e("Camera").camera(player);
    //Crafty.e("Picture").attr({x:500,y:100});

    //e.player=player;
    //e.energy=1040;
    Crafty.audio.play("music_bg",-1);
});