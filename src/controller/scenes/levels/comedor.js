/**
 * Created with JetBrains WebStorm.
 * User: pc
 * Date: 15/11/12
 * Time: 12:24 PM
 * To change this template use File | Settings | File Templates.
 */
Crafty.scene("comedor",function(){
    var books=new Array();
    var enemies=new Array();
    var pictures=new Array();
    var posi_pictures=new Array();
    var posi_enemies=new Array();
    var posi_books=new Array();
    var posi_cg=new Array();
    var posi_cgi=new Array();
    var posi_cp=new Array();
    for(var ii=0;ii<12;ii++){
        for(var jj=0;jj<6;jj++){

            Crafty.e("2D, Canvas, piso").attr({x:100*ii,y:100*jj});

        }
    }
   Crafty.e("2D, Canvas, pared1200x10").attr({x:0,y:0});
    Crafty.e("2D, Canvas, pared10x600").attr({x:0,y:0});
    Crafty.e("2D, Canvas, pared1200x10").attr({x:0,y:600});
    Crafty.e("2D, Canvas, pared10x600").attr({x:1200,y:0});


    Crafty.e("2D, Canvas, pared20x300").attr({x:900,y:10});
    Crafty.e("2D, Canvas, pared200x20").attr({x:900,y:310});
    Crafty.e("2D, Canvas, barra").attr({x:900,y:400});


    posi_pictures.push({x:80,y:370},{x:80,y:200},{x:400,y:80},{x:1050,y:550},{x:1150,y:80});
    posi_books.push({x:260,y:400},{x:350,y:80},{x:450,y:180},{x:550,y:380},{x:1100,y:500},{x:1150,y:540},{x:1100,y:210},{x:1150,y:150});
    posi_enemies.push({x:80,y:220},{x:250,y:250},{x:750,y:180}, {x:1000,y:220},{x:1100,y:150});
    posi_cg.push({x:270,y:500},{x:330,y:50},{x:330,y:120},{x:430,y:20},
        {x:370,y:500},{x:470,y:500},{x:570,y:500},{x:670,y:500}  , {x:770,y:500} ,
        {x:770,y:340},{x:470,y:340},{x:570,y:340},{x:670,y:340}  ,
        {x:470,y:440},{x:570,y:440},{x:670,y:440}  ,
        {x:400,y:250},{x:500,y:250},{x:600,y:250} ,{x:700,y:250},
        {x:60,y:500} , {x:160,y:500} ,
        {x:20,y:400} , {x:120,y:400}

    );
    posi_cgi.push({x:200,y:50},{x:200,y:200},{x:230,y:350},
        {x:300,y:50},{x:300,y:200},{x:300,y:350},
        {x:370,y:150} ,{x:370,y:250},{x:370,y:350},
        {x:440,y:370},{x:870,y:400}
    );
    posi_cp.push({x:50, y:80},{x:50, y:140},{x:50, y:200},{x:50,y:260},{x:50,y:320},
        {x:120, y:80},{x:120, y:140}, {x:120, y:200},{x:120,y:260},{x:120,y:320},{x:870,y:340});
    for(var pp=0;pp<posi_pictures.length;pp++){
        var p=Crafty.e("Picture").attr( posi_pictures[pp]);

        pictures.push(p);

    }
    for(var bb=0;bb<posi_books.length;bb++){
        var b=Crafty.e("Book").attr( posi_books[bb]);
        books.push(b);
    }


    var player = Crafty.e("Player");
    player.setPictures(pictures);
    player.arrEnem=new Array();
    for(var ee=0;ee<posi_enemies.length;ee++){
        var e=Crafty.e("Enemy").attr( posi_enemies[ee]);

        e.player=player;
        e.energy=400;
        enemies.push(e);
        player.arrEnem.push(1);

    }




    for(var cp=0;cp<posi_cp.length;cp++){
        Crafty.e("2D, Canvas, carpeta1").attr(posi_cp[cp]);


    }
    for(var cg=0;cg<posi_cg.length;cg++){
        Crafty.e("2D, Canvas, carpeta2").attr(posi_cg[cg]);


    }
    for(var cgi=0;cgi<posi_cgi.length;cgi++){
        Crafty.e("2D, Canvas, carpetagi").attr(posi_cgi[cgi]);


    }
    $("#menu").show();
    $("#marcas").html(pictures.length);
    $("#enemigos").html(enemies.length);

    Crafty.e("Camera").camera(player);
    //Crafty.e("Picture").attr({x:500,y:100});

    //e.player=player;
    //e.energy=500;
    Crafty.audio.play("music_bg",-1);
});