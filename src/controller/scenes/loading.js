/**
 * Created with JetBrains WebStorm.
 * User: pc
 * Date: 15/11/12
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
Crafty.scene("Loading",function(){

        var toLoad = [];
        toLoad.push(game_path + "resources/img/bg.jpg",
            game_path + "resources/img/bg.png",
            game_path + "resources/img/sprite.png",
            game_path + "resources/img/piso.jpg",
            game_path + "resources/img/pared10x600.png",
            game_path + "resources/img/pared20x300.png",
            game_path + "resources/img/pared200x20.png",
            game_path + "resources/img/pared20x250.png",
            game_path + "resources/img/barra.png",
            game_path + "resources/img/carpeta.png",
            game_path + "resources/img/libro.png",
            game_path + "resources/img/carpetah.png",
            game_path + "resources/img/marca.png",
            game_path + "resources/img/grilla.png",
            game_path + "resources/img/unmsm.png",
            game_path + "resources/img/count.png",
            game_path + "resources/img/grilla3.png",
            game_path + "resources/img/carpetap.png",
            game_path + "resources/img/carpetag.png" ,
            game_path + "resources/img/croquis.png" ,
            game_path + "resources/img/gameover.png" ,
            game_path + "resources/img/bienhecho.png" ,
            game_path + "resources/img/paredesh.png" ,
            game_path + "resources/img/grilla2.png",
            game_path + "resources/img/carpetagi.png");
        for(var i in Crafty.assets){
            toLoad.push(i);
        }
        //Setup background image
        Crafty.background("url("+game_path+"resources/img/bg.png) black");
        //menus

        Crafty.sprite(600,370, game_path + "resources/img/croquis.png", {

            croquis: [0,0]
        });
        Crafty.sprite(600,370, game_path + "resources/img/gameover.png", {

            gameover: [0,0]
        });
        Crafty.sprite(600,370, game_path + "resources/img/bienhecho.png", {

            bienhecho: [0,0]
        });
        Crafty.sprite(16, game_path + "resources/img/sprite.png", {

            playersprite1: [0,3]
        });
        Crafty.sprite(25, game_path + "resources/img/grilla.png", {

            playersprite: [0,0]
        });
        Crafty.sprite(25, game_path + "resources/img/grilla3.png", {

            enemy1: [0,0],
            enemy2: [0,1],
            enemy3: [0,2],
            enemy4: [0,3]
        });
        Crafty.sprite(100, game_path + "resources/img/piso.png", {
            piso: [0,0]

        });
        Crafty.sprite( 10, 600 , game_path + "resources/img/pared10x600.png", {
            pared10x600: [0,0]

        });
        Crafty.sprite(20, 300 ,game_path + "resources/img/pared20x300.png", {
            pared20x300: [0,0]

        });
        Crafty.sprite(200, 20 , game_path + "resources/img/pared200x20.png", {
            pared200x20: [0,0]

        });
        Crafty.sprite(20, 250 , game_path + "resources/img/pared20x250.png", {
            pared20x250: [0,0]

        });
        Crafty.sprite(1200, 10 , game_path + "resources/img/pared1200x10.png", {
            pared1200x10: [0,0]

        });
        Crafty.sprite(300, 30 , game_path + "resources/img/barra.png", {
            barra: [0,0]

        });

        Crafty.sprite(20, 20 , game_path + "resources/img/libro.png", {
            libro: [0,0]

        });
        Crafty.sprite(20, 20 , game_path + "resources/img/marca.png", {
            marca: [0,0]

        });
        Crafty.sprite(30 , game_path + "resources/img/carpetap.png", {
            carpeta1: [0,0]

        });
        Crafty.sprite(100 , 30 , game_path + "resources/img/carpetag.png", {
            carpeta2: [0,0]

        });
        Crafty.sprite(30 , 100 , game_path + "resources/img/carpetagi.png", {
            carpetagi: [0,0]

        });
        Crafty.sprite(200, 200 , game_path + "resources/img/count.png", {
            count321: [0,0]

        });
        Crafty.sprite(20, 20 , game_path + "resources/img/grilla2.png", {
            libro1: [0,0],
            libro2: [1,0],
            libro3: [2,0],
            libro4: [3,0],
            marca:[0,1]

        });
        Crafty.sprite(100, game_path + "resources/img/unmsm.png", {
            sistemasbtn: [0,0]
        });

        //Select DOM elements
        var bar = $('#load');
        var button = $('.button');
        var text = bar.find('.text');
        $("#menu").hide();
        $('#interface').hide();

        //Setup progressbar
        text.text("Cargando ...");

        bar.progressbar({
            value:0

        });


        //Bind click event on button
        button.live('click',function(){
            //Start scene level 1
            //Crafty.scene("sistemas");
            Crafty.scene("menus");
            //button.hide();

            //btnsist.show();
            //btncome.show();
            //btndere.show();


        });

        $('.skip').live('click',function(){
            bar.fadeOut(1000,function(){
                button.show();
            });

        });

        Crafty.load(toLoad,
            function() {
                //Everything is loaded
                bar.fadeOut(1000,function(){
                    button.show();
                });

            },
            function(e) {
                var src = e.src ||"";

                //update progress
                text.text("Cargando "+src.substr(src.lastIndexOf('/') + 1).toLowerCase()+" Loaded: "+~~e.percent+"%");
                bar.progressbar({
                    value:~~e.percent
                });


            },
            function(e) {
                //uh oh, error loading
                var src = e.src ||"";
                console.log("Error en la carga: "+src.substr(src.lastIndexOf('/') + 1).toLowerCase());
            }
        );

        Crafty.audio.add("music_bg",[
            game_path + "resources/music/nov.mp3"

        ]);

        Crafty.audio.add("l1",[ game_path + "resources/music/l2.wav"]);
        Crafty.audio.add("tick",[ game_path + "resources/music/SOUND_321.mp3"]);
        Crafty.audio.add("y1",[ game_path + "resources/music/y1.wav"]);
        Crafty.audio.add("enem",[ game_path + "resources/music/enem.mp3"]);
        //Crafty.audio.play("intro",-1);
    },
//Uninit Scene
    function(){
        //Crafty.audio.stop();

        $('#loading').hide();
    });

